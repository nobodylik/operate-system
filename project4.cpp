#define _CRT_SECURE_NO_WARNINGS
#include <math.h> 
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;
int main() {
	//freopen("input.txt", "rb", stdin);
	freopen("output.txt", "w", stdout);
	int data = 0;

	int h = 0;

	string tekstr; //текущая строка

	typedef struct Elem {
		string S;
		struct Elem *next;
	} Elem;

	Elem *head = NULL;
	Elem *pdel = NULL;
	Elem *pi = NULL;
	Elem *ppred = NULL; // указатьель на предыдущий элемент списка
	int f = 0; // флаг, как только встретим строку начинающуся с точки, сделаем единицей
	int num = 0; // номер, чтобы понять, когда нужно установить header

	while(f != 1)
	{
		cin >> tekstr;
		if (tekstr[0] == '.')
		{
			f = 1;
			break;
		}
		else
		{
			if (num == 0)
			{
				//Elem *tmp = (Elem*)malloc(sizeof(Elem));
				Elem *tmp = new Elem;
				tmp->S = tekstr;
				tmp->next = NULL;
				ppred = tmp;
				head = tmp;
				num++;
			}
			else
			{
				Elem *tmp = new Elem;
				tmp->S = tekstr;
				tmp->next = NULL;
				ppred->next = tmp;
				ppred = tmp;
			}
		}		
	}

	pi = head;
	while (pi != NULL)
	{
		cout << pi->S << " ";

		pdel = pi;
		pi = pi->next;
		free(pdel);
	}

}